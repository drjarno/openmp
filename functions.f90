program functions
use omp_lib

real*8 starttime

starttime = omp_get_wtime();

write(*,*) "Number of processors available   :", &
  & omp_get_num_procs()
write(*,*) "Maximum number of threads allowed:", &
  & omp_get_max_threads()

!$omp parallel
!$omp critical
write(*,*) "Thread num :", omp_get_thread_num()
!$omp end critical
!$omp end parallel

write(*,*) "Program took", &
  & omp_get_wtime() - starttime, "seconds"

end program
