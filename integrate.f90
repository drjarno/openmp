function integrate(a, b, resolution)
  real*8 :: integrate
  real :: a, b
  integer :: resolution
  real*8 :: dx, x, total
  integer :: i
  real*8, parameter :: pi = 3.141592653589793238462643383279502884197

  dx = (b - a) / resolution
  total = 0;

  do i = 0, resolution
    x = a + i*dx
    total = total + exp(sin(pi*x)-(x**2.5))
  end do

  integrate = total*dx
  return
end function integrate

program convergence
  implicit none
  real*8 :: integrate
  real*8 :: res
  integer :: n

  !$omp parallel do schedule(runtime)
  do n = 1, 9*4
    res = integrate(0., 1., ceiling(10**(n/4.)))
  end do
  !$omp end parallel do

end program convergence
