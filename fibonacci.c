#include <stdio.h>

double fib(int n) {
  double Fn, Fnmin1, Fnmin2;
  int i;
  
  Fnmin2 = 0;
  Fnmin1 = 1;

  if(n == 0)
    return 0;
  else if(n == 1)
    return 1;

  for(i = 0; i < n-1; i++) {
    Fn = Fnmin1 + Fnmin2;
    Fnmin2 = Fnmin1;
    Fnmin1 = Fn;
  }

  return Fn;
}

int main() {
  int i = 0;

  printf("%.0lf\n", fib(100));

  return 0;
}
