#include <stdio.h>
#include <stdlib.h>

#define NUM_ELEMENTS 100

int main() {
  int a[NUM_ELEMENTS];
  int i;
  int total;

  for(i = 0; i < NUM_ELEMENTS; i++)
    a[i] = 2*i;

  total = 0;
#pragma omp parallel for
  for(i = 0; i < NUM_ELEMENTS; i++)
    // This is bad because the shared variable "total" is
    // written by multiple threads, overwriting each other.
    total += a[i];
  printf("Total = %d\n", total);

#pragma omp parallel for
  for(i = 0; i < NUM_ELEMENTS-1; i++)
    // This is bad because a[i+1] can be changed by another
    // thread whereas for serial execution it cannot.
    a[i] = (a[i] - a[i+1]) / 2;

  for(i = 0; i < NUM_ELEMENTS; i++)
    printf("%d ", a[i]);
  printf("\n");

  return EXIT_SUCCESS;
}
