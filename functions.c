#include <stdio.h>
#include <omp.h>

int main() {
  double starttime = omp_get_wtime();

  printf("Number of processors available   : %d\n",
    omp_get_num_procs());
  printf("Maximum number of threads allowed: %d\n",
    omp_get_max_threads());

#pragma omp parallel
  {
#pragma omp critical
    printf("Thread num : %d\n", omp_get_thread_num());
  }

  printf("Program took %lf seconds\n",
    omp_get_wtime() - starttime);

  return 0;
}
