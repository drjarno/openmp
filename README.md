[français](#markdown-header-seminaire-openmp) | [english](#markdown-header-openmp-seminar)
# Séminaire OpenMP
Ce sont les fichiers pour le séminaire OpenMP de l'Université d'Ottawa. Il y a quatre fichiers d'exemple ainsi qu'un Makefile pour la compilation.
## bad.c
Cet exemple montre ce qui peut donner de mauvais résultats si votre code n'est pas à fil sécurisé.
## fibonacci.c
Cet exemple montre un algorithme qui est impossible d'éxecuter en parallèle parce que chaque itération dépend des itérations précédents.
## geometric.(f90|c)
Ce montre du code parallel pour calculer la série géométrique x^n pour diverses valeurs de x. Les données sont enregistrées dans le fichier « output.dat » afin que vous pouvez tracer un graphique et vous pouvez le comparer à 1/(1-x) qui est la solution analytique.

Voyez la différence dans la durée d'exécution en faisant l'éxecuter :

    OMP_NUM_THREADS=4 time ./geometric

Changez le nombre 4 à d'autres valeurs pour voir l'effet à la durée d'éxecution.
## integrate.(f90|c)
Cet exemple montre l'impact du planificateur qui détermine comment la manière de la division des tâches.

Voyez la différence dans la durée d'exécution en faisant l'éxecuter (sur Linux ou macOS) :

    OMP_SCHEDULE=static time ./integrate

Changez « static » à « guided » ou « dynamic » pour voir l'effet à la durée d'éxecution.
## reduction.cpp
Ce montre la fonctionnalité réduire que est présent à OpenMP. Ce permet tous les fils pour accéder les variables partagés sûrement et efficacement.
## Compiler
Un Makefile est ajouté pour la compilation sur Linux. Pour l'utiliser, tapez

    make

# OpenMP seminar
These are the files for the uOttawa OpenMP seminar. There are four example files as well as a Makefile for compilation
## bad.c
This example demonstrates what can go wrong when your code is not thread-safe.
## fibonacci.c
This example demonstrates an algorithm that cannot be made to run in parallel because each iteration depends on the previous ones.
## geometric.(f90|c)
This demonstrate parallel code for calculating the geometric series x^n for various values of x. The data is written to "output.dat" so you can plot it and compare it to 1/(1-x) which is the analytical solution.

See the difference in run time by running it (on Linux or macOS) as

    OMP_NUM_THREADS=4 time ./geometric

And change the number 4 to other values to see the impact on runtime.
## integrate.(f90|c)
This example demonstrates the impact of the scheduler which determines how the task are divided up.

See the difference in run time by running it (on Linux or macOS) as

    OMP_SCHEDULE=static time ./integrate

And change "static" to other "guided" or "dynamic" to see the impact on runtime.
## reduction.cpp
This demonstrates the reduce functionality that comes with OpenMP. This allows all threads to access the shared variables safely and efficiently.
## Compiling
A Makefile is included for compilation on Linux. To use, type

    make
