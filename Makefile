all: bad geometric reduction fibonacci functions integrate

clean:
	$(RM) bad geometric_c geometric_f reduction fibonacci functions_c functions_f integrate_c integrate_f

bad: bad.c
	$(CC) bad.c -o bad -fopenmp

geometric: geometric_c geometric_f

geometric_c: geometric.c
	$(CC) geometric.c -o geometric_c -fopenmp -lm

geometric_f: geometric.f90
	$(FC) geometric.f90 -o geometric_f -fopenmp

reduction: reduction.cpp
	$(CXX) reduction.cpp -o reduction -fopenmp

fibonacci: fibonacci.c
	$(CC) fibonacci.c -o fibonacci -fopenmp -lm

functions: functions_c functions_f

functions_c: functions.c
	$(CC) functions.c -o functions_c -fopenmp -lm

functions_f: functions.f90
	$(FC) functions.f90 -o functions_f -fopenmp

integrate: integrate_c integrate_f

integrate_c: integrate.c
	$(CC) integrate.c -o integrate_c -fopenmp -lm

integrate_f: integrate.f90
	$(FC) integrate.f90 -o integrate_f -fopenmp

.PHONY: geometric functions integrate
