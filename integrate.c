#include <stdio.h>
#include <math.h>

double integrate(double a, double b, int resolution) {
  double dx = (b - a) / resolution;
  double x;
  double total = 0.;

  for(x = a; x <= b; x += dx)
  {
    total += exp(sin(M_PI*x)-pow(x,2.5));
  }

  return total*dx;
}

int main() {
  int n = 0;
  double res;

 #pragma omp parallel for schedule(runtime)
  for(n = 1; n <= 9*4; n++) {
    res = integrate(0., 1., ceil(pow(10, n/4.)));
  }

  return 0;
}
