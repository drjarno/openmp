#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NUM_ELEMENTS  5000
#define NUM_TERMS     100000
#define DX            0.0001

int main() {
  int i, j;
  double elements[NUM_ELEMENTS];
  FILE *f;

#pragma omp parallel for
  for(i = 0; i < NUM_ELEMENTS; i++) {
    elements[i] = 0;
    for(j = 0; j < NUM_TERMS; j++) {
      elements[i] += pow(i*DX, j);
    }
  }

  f = fopen("output.dat", "w");
  if(f) {
    for(i = 0; i < NUM_ELEMENTS; i++)
      fprintf(f, "%f\t%.16lf\n", i*DX, elements[i]);
    fclose(f);
  }

  return EXIT_SUCCESS;
}
