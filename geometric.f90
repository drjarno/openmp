program openmp

integer, parameter :: num_elements = 5000
integer, parameter :: num_terms = 100000
real, parameter :: dx = 0.0001
integer :: i, j
real*8 :: elements(num_elements)

!$omp parallel do
do i = 1, num_elements
  elements(i) = 0
  do j = 1, num_terms
    elements(i) = elements(i) + (i*dx) ** j
  end do
end do
!$omp end parallel do

open(1, file='output.dat')
do i = 1, num_elements
  write(1,*) i*dx, elements(i)
end do

close(1)

end program
