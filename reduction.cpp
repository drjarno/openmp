#include <iostream>
#include <vector>
#include <utility>
#include <random>
#include <cmath>

int main() {
  const int num_attempts = 50000000;

  std::default_random_engine generator;
  std::uniform_real_distribution<double> dist(0, 1);
  std::vector<std::pair<double,double>> array(num_attempts);

  // C++ random generator is not thread-safe
  for(auto &a : array) {
    a.first = dist(generator);
    a.second = dist(generator);
  }

  std::cout << array[0].first << ", " << array[0].second << std::endl;

  int total = 0;
#pragma omp parallel
  {
#pragma omp for reduction(+:total)
    // OpenMP does not work with C++-style iterators, so use int
    for(std::size_t i = 0; i < array.size(); i++) {
      double r2 = pow(array[i].first, 2) + pow(array[i].second, 2);
      if(r2 <= 1.)
        total++;
    }
  }

  std::cout << "Pi = " << 4*static_cast<double>(total) / num_attempts << std::endl;

  return 0;
}
